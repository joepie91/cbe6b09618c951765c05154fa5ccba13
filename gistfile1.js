/// Handler for /travel/{url}
module.exports.travel = (event, context, callback) => {
  // Get the url from the resource path.
  // Get the html.
  // Turn it into a successful request.
  // Attach the Node.js callback onto the end.

  return Promise.try(() => {
    return getURLFromPath(event.path);
  }).then((url) => {
    return getFromURL(url);
  }).then((response) => {
    return make200Response(response);
  }).asCallback(callback);
}